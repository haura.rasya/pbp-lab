from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

def index(request):
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
      
    if request.method == 'POST':
        form = NoteForm(request.POST) 

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
    else:
        form = NoteForm()

    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_note_list.html', response)