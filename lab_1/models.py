from django.db import models

class Friend(models.Model):
    name = models.CharField(max_length=30, blank=True)
    npm = models.CharField(max_length=30, blank=True)
    DOB = models.DateField()
