from django.db import models

class Note(models.Model):
    to = models.CharField(max_length=50)
    message_from = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    message = models.TextField()
