import 'package:flutter_complete_guide/models/question.dart';
import 'quiz.dart';

class Answer{
  String text;
  bool correct;
  Question question;

  Answer({this.text, this.correct, this.question});

  static List<Answer>getAnswer(){
    Quiz q = Quiz(name: "math", topic: "penjumlahan", numberOfQuestion: 3, time: 3, requiredScoreToPass: 33);
    Question quest = Question(text: "2+2", quiz: q);
    return <Answer>[
      Answer(text: "4", correct: true, question: quest),
      Answer(text: "5", correct: false, question: quest),
      Answer(text: "6", correct: false, question: quest),
      Answer(text: "7", correct: false, question: quest),
    ];
  }
}