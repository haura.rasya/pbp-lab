import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/quiz.dart';

class EditQuestionScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return EditQuestionScreenState();
  }
}

class EditQuestionScreenState extends State<EditQuestionScreen> {

  String _text;
  Quiz _quiz;

  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final List<Quiz> quiz = Quiz.getQuiz();

  Widget _buildTextField(){
    return TextFormField(
      decoration: InputDecoration(labelText: 'Text'),
      validator: (String val){
        if(val.isEmpty){
          return 'Text is required';
        }
        return null;
      },
      onSaved: (String val){
        _text = val;
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Edit Question",
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(24),
        // color: Theme.of(context).primaryColor,
        child: Form(
          key: _form,
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildTextField(),
            DropdownButtonFormField(
              value: quiz[0],
              items: quiz.map((q){
                return DropdownMenuItem(
                  value: q,
                  child: Text('${q.name}-${q.topic}')
                );
              }).toList(),

              onChanged: (Quiz val){
                _quiz = val;
              },

              validator: (Quiz q){
                if(_quiz == null) return "Quiz is required";
                return null;
              }
            ),
            SizedBox(height: 45),
            SizedBox(
              height: 38,
              width: 100,
              child: RaisedButton(
                color: Colors.purple,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
                child: Text(
                  'Update', 
                  style: TextStyle(
                    color: Colors.white, 
                    fontSize: 16,
                  ),
                ),
                onPressed: () {
                  if (!_form.currentState.validate()){
                    return;
                  }

                  _form.currentState.save();
                  print(_text);
                  print(_quiz);
                },
              ),
            )
          ],
        ),),
      ),
    );
  }
}