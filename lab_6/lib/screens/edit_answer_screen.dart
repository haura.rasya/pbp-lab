import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/question.dart';

class EditAnswerScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return EditAnswerScreenState();
  }
}

class EditAnswerScreenState extends State<EditAnswerScreen> {

  String _text;
  bool _correct = false;
  Question _question;

  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final List<Question> questions = Question.getQuestion();

  Widget _buildTextField(){
    return TextFormField(
      decoration: InputDecoration(labelText: 'Text'),
      validator: (String val){
        if(val.isEmpty){
          return 'Text is required';
        }
        return null;
      },
      onSaved: (String val){
        _text = val;
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Edit Answer",
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(40),
        // color: Theme.of(context).primaryColor,
        child: Form(
          key: _form,
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildTextField(),
            DropdownButtonFormField(
              value: questions[0],
              // value: _question ?? questions.getRange(0, 1),
              items: questions.map((q){
                return new DropdownMenuItem(
                  value: q,
                  child: Text('${q.text}')
                );
              }).toList(),

              onChanged: (Question q){
                setState(() {
                  _question = q;
                });
              },

              validator: (Question q){
                if(_question == null) return "Question is required";
                return null;
              }
            ),
            SizedBox(
              height: 90,
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: CheckboxListTile(
                    title: Text('Correctness'),
                    subtitle: Text('Is this the correct answer?'),
                    secondary: Icon(Icons.check),
                    checkColor: Colors.white,
                    controlAffinity: ListTileControlAffinity.trailing,
                    value: _correct,
                    selected: _correct,
                    onChanged: (bool val){
                      setState(() {
                        _correct = val;
                      });
                    },
                  ),
                ),
              ),
            ),
            
            SizedBox(height: 40),
            SizedBox(
              height: 38,
              width: 100,
              child: RaisedButton(
                color: Colors.purple,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
                child: Text(
                  'Update', 
                  style: TextStyle(
                    color: Colors.white, 
                    fontSize: 16,
                  ),
                ),
                onPressed: () {
                  if (!_form.currentState.validate()){
                    return;
                  }

                  _form.currentState.save();
                  print(_text);
                  print(_question);
                  print(_correct);
                },
              ),
            )
          ],
        ),),
      ),
    );
  }
}