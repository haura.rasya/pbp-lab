import 'package:flutter/material.dart';

class EditQuizScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return EditQuizScreenState();
  }
}

class EditQuizScreenState extends State<EditQuizScreen> {

  String _name;
  String _topic;
  String _number_of_question;
  String _time;
  String _required_score;

  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  Widget _buildNameField(){
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      validator: (String val){
        if(val.isEmpty){
          return 'Name is required';
        } return null;
        // return val;
      },
      onSaved: (String val){
        _name = val;
      },
    );
  }

  Widget _buildTopicField(){
    return TextFormField(
      decoration: InputDecoration(labelText: 'Topic'),
      validator: (String val){
        if(val.isEmpty){
          return 'Topic is required';
        } return null;
      },
      onSaved: (String val){
        _topic = val;
      },
    );
  }

  Widget _buildNumberField(){
    return TextFormField(
      decoration: InputDecoration(labelText: 'Number of question'),
      keyboardType: TextInputType.number,
      validator: (String val){

        int num = int.tryParse(val);

        if(num == null || num < 0){
          return 'Number of question must be greater than 0';
        } return null;
      },
      onSaved: (String val){
        _number_of_question = val;
      },
    );
  }

  Widget _buildTimeField(){
    return TextFormField(
      decoration: InputDecoration(labelText: 'Time Limit in Minutes'),
      keyboardType: TextInputType.number,
      validator: (String val){

        int num = int.tryParse(val);

        if(num == null || num < 0){
          return 'Time limit must be greater than 0';
        } return null;
      },
      onSaved: (String val){
        _time = val;
      },
    );
  }

  Widget _buildScoreField(){
    return TextFormField(
      decoration: InputDecoration(labelText: 'Required Score to Pass'),
      keyboardType: TextInputType.number,
      validator: (String val){

        int num = int.tryParse(val);

        if(num == null || num < 0){
          return 'Score must be greater than 0';
        } return null;
      },
      onSaved: (String val){
        _number_of_question = val;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Edit Quiz",
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(24),
        child: Form(
          key: _form,
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildNameField(),
            _buildTopicField(),
            _buildNumberField(),
            _buildTimeField(),
            _buildScoreField(),
            SizedBox(height: 40),
            SizedBox(
              height: 38,
              width: 100,
              child: RaisedButton(
                color: Colors.purple,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
                child: Text(
                  'Update', 
                  style: TextStyle(
                    color: Colors.white, 
                    fontSize: 16,
                  ),
                ),
                onPressed: () {
                  if (!_form.currentState.validate()){
                    return;
                  }

                  _form.currentState.save();
                  print(_name);
                },
              ),
            )
          ],
        ),),
      ),
    );
  }
}