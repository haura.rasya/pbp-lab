import './screens/question_list_screen.dart';
import './screens/quiz_list_screen.dart';
import './screens/quiz_list_siswa.dart';
import './screens/answer_list_screen.dart';

import './screens/form_screen.dart';
import './screens/add_answer_screen.dart';
import './screens/add_question_screen.dart';
import './screens/edit_question_screen.dart';
import './screens/edit_quiz_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Quiz',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      // home: FormScreen(),
      // home: AddQuestionScreen(),
      home: QuizListScreen(),
      // home: EditQuestionScreen(),
      // home: EditQuizScreen(),
      // home: QuestionListScreen(),
      // home: AnswerListScreen(),
      // home: AddAnswerScreen(),
      // home: QuizListSiswaScreen(),
    );
  }
}