import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/answer.dart';
import 'package:flutter_complete_guide/models/quiz.dart';
import 'package:flutter_complete_guide/screens/add_answer_screen.dart';
import 'package:flutter_complete_guide/screens/edit_answer_screen.dart';

class AnswerListScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState(){
    return AnswerListScreenState();
  }
}

class AnswerListScreenState extends State<AnswerListScreen>{

  List<Answer> answers;
  List<Answer> selectedAnswers;

  @override
  void initState(){
    answers = Answer.getAnswer();
    selectedAnswers = [];
    super.initState();
  }

  selectedRow(bool selected, Answer answer) async{

    setState(() {
      if (selected){
        selectedAnswers.add(answer);
      }
      else{
        selectedAnswers.remove(answer);
      } 
    });

  }

  deleteSelected() async{
    setState(() {
      if(selectedAnswers.isNotEmpty){
        List<Answer> tmp = [];
        tmp.addAll(selectedAnswers);
        for (Answer q in tmp){
          answers.remove(q);
          selectedAnswers.remove(q);
        }
      }
    });
  }

  Widget buildButton() => FloatingActionButton.extended(
    icon: Icon(Icons.add),
    label: Text('Add Answer'),
    backgroundColor: Colors.purple,
    foregroundColor: Colors.white,
    onPressed: (){
      print("add quiz");
      Navigator.push(context, 
        MaterialPageRoute(builder: (context) => AddAnswerScreen()),
      );
    },
  );

  DataTable answerList(){
    return DataTable(
      columns: [
        DataColumn(
          label: Text("TOPIC"),
          numeric: false,
          tooltip: "Topic of The Quiz",
        ),
        DataColumn(
          label: Text("CORRECT"),
          numeric: false,
          tooltip: "Correctness",
        ),
        DataColumn(
          label: Text("EDIT"),
          numeric: false,
          tooltip: "Edit Answer",
        ),
      ], 
      rows: answers
          .map(
            (answer) => DataRow(
              selected: selectedAnswers.contains(answer),
              onSelectChanged: (b) {
                selectedRow(b, answer);
              },
              cells: [
                DataCell(
                  Text(answer.text),
                  onTap: (){
                    print('${answer.text}');
                  }
                ),
                DataCell(
                  Text('${answer.correct}'),
                  onTap: (){
                    print('${answer.correct}');
                  }
                ),
                DataCell(
                  Icon(
                    Icons.edit,
                  ),
                  onTap: (){
                    print('${answer.question.quiz.name} - ${answer.question.quiz.topic}');
                    Navigator.push(context, 
                      MaterialPageRoute(builder: (context) => EditAnswerScreen()),
                    );
                  }
                ),
              ]
            ),
          ).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Answer List"),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        verticalDirection: VerticalDirection.down,
        children: <Widget>[
          Center(
            child: answerList(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

              Padding(
                padding: EdgeInsets.all(20.0),
                child: OutlinedButton(
                  child: Text("Selected: ${selectedAnswers.length}"),
                  onPressed: () {},
                ),
              ),
              
              Padding(
                padding: EdgeInsets.all(15.0),
                child: OutlinedButton(
                  child: Text("Delete Selected"),
                  onPressed:selectedAnswers.isEmpty ? null: () {
                    deleteSelected();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
      floatingActionButton: buildButton(),
    );
  }
}