import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/quiz.dart';
import 'package:flutter_complete_guide/screens/add_question_screen.dart';
import 'package:flutter_complete_guide/screens/edit_quiz_screen.dart';
import 'package:flutter_complete_guide/screens/form_screen.dart';
import 'package:flutter_complete_guide/screens/question_list_screen.dart';

class QuizListScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return QuizListScreenState();
  }
}

class QuizListScreenState extends State<QuizListScreen>{

  List<Quiz> quizzes;
  List<Quiz> selectedQuizzes;

  @override
  void initState(){
    quizzes = Quiz.getQuiz();
    selectedQuizzes = [];
    super.initState();
  }

  selectedRow(bool selected, Quiz quiz) async{

    setState(() {
      if (selected){
        selectedQuizzes.add(quiz);
      }
      else{
        selectedQuizzes.remove(quiz);
      } 
    });

  }

  deleteSelected() async{
    setState(() {
      if(selectedQuizzes.isNotEmpty){
        List<Quiz> tmp = [];
        tmp.addAll(selectedQuizzes);
        for (Quiz q in tmp){
          quizzes.remove(q);
          selectedQuizzes.remove(q);
        }
      }
    });
  }

  Widget buildButton() => FloatingActionButton.extended(
    icon: Icon(Icons.add),
    label: Text('Add Quiz'),
    backgroundColor: Colors.purple,
    foregroundColor: Colors.white,
    onPressed: (){
      print("add quiz");
      Navigator.push(context, 
        MaterialPageRoute(builder: (context) => FormScreen()),
      );
    },
  );

  DataTable quizList(){
    return DataTable(
      columns: [
        DataColumn(
          label: Text("TOPIC"),
          numeric: false,
          tooltip: "Topic of The Quiz",
        ),
        DataColumn(
          label: Text("ADD"),
          numeric: false,
          tooltip: "Add Question",
        ),
        DataColumn(
          label: Text("EDIT"),
          numeric: false,
          tooltip: "Edit Quiz",
        ),
      ], 
      rows: quizzes
          .map(
            (quiz) => DataRow(
              selected: selectedQuizzes.contains(quiz),
              onSelectChanged: (b) {
                selectedRow(b, quiz);
              },
              cells: [
                DataCell(
                  Text(quiz.topic),
                  onTap: (){
                    print('${quiz.topic}');
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => QuestionListScreen()),
                    );
                  }
                ),
                DataCell(
                  Icon(
                    Icons.add,
                    color: Colors.black,
                  ),
                  onTap: (){
                    print('${quiz.topic}');
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => AddQuestionScreen()),
                    );
                  }
                ),
                DataCell(
                  Icon(
                    Icons.edit,
                  ),
                  onTap: (){
                    print('${quiz.topic}');
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => EditQuizScreen()),
                    );
                  }
                ),
              ]
            ),
          ).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Quiz List"),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        verticalDirection: VerticalDirection.down,
        children: <Widget>[
          Center(
            child: quizList(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

              Padding(
                padding: EdgeInsets.all(20.0),
                child: OutlinedButton(
                  child: Text("Selected: ${selectedQuizzes.length}"),
                  onPressed: () {},
                ),
              ),
              
              Padding(
                padding: EdgeInsets.all(15.0),
                child: OutlinedButton(
                  child: Text("Delete Selected"),
                  onPressed:selectedQuizzes.isEmpty ? null: () {
                    deleteSelected();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
      floatingActionButton: buildButton(),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}