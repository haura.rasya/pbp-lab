import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/question.dart';
import 'package:flutter_complete_guide/screens/add_question_screen.dart';
import 'package:flutter_complete_guide/screens/answer_list_screen.dart';
import 'package:flutter_complete_guide/screens/edit_question_screen.dart';


class QuestionListScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return QuestionListScreenState();
  }
}

class QuestionListScreenState extends State<QuestionListScreen>{

  List<Question> questions;
  List<Question> selectedQuestions;

  @override
  void initState(){
    questions = Question.getQuestion();
    selectedQuestions = [];
    super.initState();
  }

  selectedRow(bool selected, Question question) async{

    setState(() {
      if (selected){
        selectedQuestions.add(question);
      }
      else{
        selectedQuestions.remove(question);
      } 
    });

  }

  deleteSelected() async{
    setState(() {
      if(selectedQuestions.isNotEmpty){
        List<Question> tmp = [];
        tmp.addAll(selectedQuestions);
        for (Question q in tmp){
          questions.remove(q);
          selectedQuestions.remove(q);
        }
      }
    });
  }

  Widget buildButton() => FloatingActionButton.extended(
    icon: Icon(Icons.add),
    label: Text('Add Question'),
    backgroundColor: Colors.purple,
    foregroundColor: Colors.white,
    onPressed: (){
      print("add quiz");
      Navigator.push(context, 
        MaterialPageRoute(builder: (context) => AddQuestionScreen()),
      );
    },
  );

  DataTable questionList(){
    return DataTable(
      columns: [
        DataColumn(
          label: Text("TEXT"),
          numeric: false,
          tooltip: "List of Questions",
        ),
        DataColumn(
          label: Text("Answer"),
          numeric: false,
          tooltip: "View Answer",
        ),
      ], 
      rows: questions
          .map(
            (question) => DataRow(
              selected: selectedQuestions.contains(question),
              onSelectChanged: (b) {
                selectedRow(b, question);
              },
              cells: [
                DataCell(
                  Text(question.text),
                  onTap: (){
                    print('${question.text}');
                    Navigator.push(context, 
                      MaterialPageRoute(builder: (context) => EditQuestionScreen()),
                    );
                  }
                ),
                DataCell(
                  Icon(
                    Icons.view_list
                  ),
                  onTap: (){
                    print('${question.text}');
                    Navigator.push(context, 
                      MaterialPageRoute(builder: (context) => AnswerListScreen()),
                    );
                  }
                ),
              ]
            ),
          ).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Question List"),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        verticalDirection: VerticalDirection.down,
        children: <Widget>[
          Center(
            child: questionList(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(20.0),
                child: OutlinedButton(
                  child: Text("Selected: ${selectedQuestions.length}"),
                  onPressed: () {},
                ),
              ),

              Padding(
                padding: EdgeInsets.all(20.0),
                child: OutlinedButton(
                  child: Text("Delete Selected"),
                  onPressed:selectedQuestions.isEmpty ? null: () {
                    deleteSelected();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
      floatingActionButton: buildButton(),
    );
  }
}