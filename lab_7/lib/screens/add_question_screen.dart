import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/quiz.dart';

class AddQuestionScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return AddQuestionScreenState();
  }
}

class AddQuestionScreenState extends State<AddQuestionScreen> {

  String _text;
  Quiz _quiz;

  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final List<Quiz> quiz = Quiz.getQuiz();

  Widget _buildTextField(){
    return TextFormField(
      decoration: InputDecoration(labelText: 'Text'),
      validator: (String val){
        if(val.isEmpty){
          return 'Text is required';
        }
        return null;
      },
      onSaved: (String val){
        _text = val;
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Add Question",
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(24),
        // color: Theme.of(context).primaryColor,
        child: Form(
          key: _form,
          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildTextField(),
            DropdownButtonFormField(
              value: quiz[0],
              items: quiz.map((q){
                return DropdownMenuItem(
                  value: q,
                  child: Text('${q.name}-${q.topic}')
                );
              }).toList(),

              onChanged: (Quiz val){
                setState(() {
                  _quiz = val;
                });
              },

              validator: (Quiz val){
                if(_quiz == null) return "Quiz is required";
                return null;
              }
            ),
            SizedBox(height: 40,),
            SizedBox(
              height: 38,
              width: 100,
              child: RaisedButton(
                color: Colors.purple,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30))),
                child: Text(
                  'Submit', 
                  style: TextStyle(
                    color: Colors.white, 
                    fontSize: 16,
                  ),
                ),
                onPressed: () {
                  if (!_form.currentState.validate()){
                    return;
                  }

                  _form.currentState.save();
                  print(_text);
                  print(_quiz);
                },
              ),
            )
          ],
        ),),
      ),
    );
  }
}