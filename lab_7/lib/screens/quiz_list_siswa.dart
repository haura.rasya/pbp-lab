import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/quiz.dart';

class QuizListSiswaScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return QuizListSiswaScreenState();
  }
}

class QuizListSiswaScreenState extends State<QuizListSiswaScreen>{

  List<Quiz> quizzes;

  @override
  void initState(){
    quizzes = Quiz.getQuiz();
    super.initState();
  }

  DataTable quizList(){
    return DataTable(
      columns: [
        DataColumn(
          label: Text("TOPIC"),
          numeric: false,
          tooltip: "Topic of The Quiz",
        ),
        DataColumn(
          label: Text("DURATION"),
          numeric: false,
          tooltip: "Duration on Minute",
        ),
        DataColumn(
          label: Text("ATTEMPT"),
          numeric: false,
          tooltip: "Start Attempt",
        ),
      ], 
      rows: quizzes
          .map(
            (quiz) => DataRow(
              cells: [
                DataCell(
                  Text(quiz.topic),
                  onTap: (){
                    print('${quiz.topic}');
                  }
                ),
                DataCell(
                  Text(quiz.time.toString()),
                  onTap: (){
                    print('${quiz.time}');
                  }
                ),
                DataCell(
                  Icon(
                    Icons.smart_display_outlined,
                    color: Colors.purple,
                  ),
                  onTap: (){
                    print('${quiz.topic}');
                  }
                ),
              ]
            ),
          ).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Quiz List"),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        verticalDirection: VerticalDirection.down,
        children: <Widget>[
          Center(
            child: quizList(),
          ),
        ],
      ),
    );
  }
}