class Quiz{
  String name;
  String topic;
  int numberOfQuestion;
  int time;
  int requiredScoreToPass;

  Quiz({this.name, this.topic, this.numberOfQuestion, this.time, this.requiredScoreToPass});

  static List<Quiz>getQuiz(){
    return <Quiz>[
      Quiz(name: "math", topic: "penjumlahan", numberOfQuestion: 3, time: 3, requiredScoreToPass: 33),
      Quiz(name: "math", topic: "pengurangan", numberOfQuestion: 4, time: 6, requiredScoreToPass: 40),
      Quiz(name: "math", topic: "perkalian", numberOfQuestion: 5, time: 7, requiredScoreToPass: 41),
      Quiz(name: "math", topic: "pembagian", numberOfQuestion: 6, time: 8, requiredScoreToPass: 44),
      Quiz(name: "math", topic: "pemangkatan", numberOfQuestion: 7, time: 9, requiredScoreToPass: 48),
      Quiz(name: "math", topic: "pengintegralan", numberOfQuestion: 8, time: 10, requiredScoreToPass: 50),
    ];
  }
}