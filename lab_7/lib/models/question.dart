import 'package:flutter_complete_guide/models/quiz.dart';

class Question{
  String text;
  Quiz quiz;

  Question({this.text, this.quiz});

  static List<Question>getQuestion(){
    Quiz q = Quiz(name: "math", topic: "penjumlahan", numberOfQuestion: 3, time: 3, requiredScoreToPass: 33);
    return <Question>[
      Question(text: "2+2", quiz: q),
      Question(text: "3+2", quiz: q),
      Question(text: "4+2", quiz: q),
      Question(text: "5+2", quiz: q)
    ];
  }
}