import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomAlert extends StatelessWidget{

  String name;
  String topic;
  String numberOfQuestion;
  String time;
  String requiredScoreToPass;

  CustomAlert({this.name, this.topic, this.numberOfQuestion, this.time, this.requiredScoreToPass});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      ),
      child: Stack(
        overflow: Overflow.visible,
        alignment: Alignment.topCenter,
        children: [
          Container(
            height: 280,
            child: Padding(
              padding: EdgeInsets.fromLTRB(10, 70, 10, 10),
              child: Column(
                children: [
                  Text('Quiz has been added successfully',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 8,),
                  Text('Name : $name',
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 5,),
                  Text('Topic : $topic',
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 5,),
                  Text('Number of Questions : $numberOfQuestion',
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 5,),
                  Text('Time Limit : $time minutes',
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 5,),
                  Text('Required score : $requiredScoreToPass',
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 20,),
                  RaisedButton(onPressed: (){
                    Navigator.of(context).pop();
                  },
                  color: Colors.purple,
                  shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))),
                  child: Text('Done', style: TextStyle(color: Colors.white),),
                  )
                ],
              ),
            ),
          ),
          Positioned(child: CircleAvatar(
            backgroundColor: Colors.purple,
            radius: 50,
            child: Icon(Icons.check, size: 40, color: Colors.white,),
          ),
          top: -50,
          )
        ],
      ),
    );
  }
}