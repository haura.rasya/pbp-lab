from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    context ={}
      
    if request.method == 'POST':
        form = FriendForm(request.POST) 

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')
    else:
        form = FriendForm()

    context['form']= form
    return render(request, "lab3_form.html", context)